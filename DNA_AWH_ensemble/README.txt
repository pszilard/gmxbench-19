Background
----------
The DNA system has been used for studying DNA base pair opening and the importance
of the local and global DNA sequence. [1].
Calculating the free energy for all base pairs in a sequence and for a large database
of sequences of interest leads to a large problem space (in the hundreds to thousands)
that makes this an inherently large-scale and parallel problem.
The individual MD simulations for a specific base pair and sequence can themselves 
composed of an ensemble computation with 16-32 simulations using the AWH method.
The inputs of this benchmark represent such a base pair and sequence combination.

This system contains approximately 45000 atoms which is challenging for strong
scaling and it is unlikely to scale well beyond a single modern compute node.

The simulation uses 2 fs time steps and hydrogen bond constraints.


[1] Viveca Lindahl, Alessandra Villa, and Berk Hess. Sequence dependency of canonical
base pair opening in the dna double helix. PLoS computational biology, 13(4):e1005463,
2017.


Preparation
-----------
As the AWH ensemble run is flexible in terms of ensemble size. For this benchmark
create a 32-member ensemble run. As these use identical input files,
we set up a tpr input, than create output directories for each member of the
ensemble for the mdrun "multidir" feature and symlink the tpr input in each of
the created directories e.g. as done in the below bash shell example code:

gmx grompp -n index.ndx -o start.tpr
for i in {01..32}; do
  dirname=repl_${i}
  ( mkdir $dirname && cd $dirname && ln -s ../start.tpr topol.tpr; )
done

Launching runs
--------------
To launch the AWH ensemble computation, launch N*32 ranks and pass
the 32 ensemble output directories to the mdrun "-multidir" options.
Set a runtime length in wall-time and use counter resetting to
record performance after the load balancing phase is complete and
the hardware "warms up". An example command for a 32-rank run of 30 minutes
(i.e. one rank per simulation and the last 15 minutes used for measurement).

mpirun -np 32 gmx_mpi mdrun -multidir repl_* -nsteps -1 -maxh 0.5 -resethway
